bnjmnmlist
##########

I originally started my site using the pelican 
`mnmlist theme <https://github.com/getpelican/pelican-themes/tree/master/mnmlist/>`_.
Over time, the templates and styling have changed significantly to 
fit my liking. Although the overall style is still minimal, the functionality 
has been improved and readability enhanced, in my opinion.

Settings
~~~~~~~~

This theme has been hardcoded with a Creative Commons Attribution-NonCommercial-ShareAlike 
3.0 Unported License. This content license is optional but if you desire a different
license a fork and modification of the theme is required. If you want it, just include the
following settings:

:: 

    COPYRIGHT = (True|False)
    COPYRIGHT_YEAR = u'2010-2013'
    COPYRIGHT_HOLDER = u'Jimmy Struthers'

That's all for now. 

Compass
~~~~~~~

The ``main.css`` file is generated from the ``compass/src/main.scss`` sass file, using http://compass-style.org/.


Development Setup
~~~~~~~~~~~~~~~~~

Install ``compass``

    sudo gem install compass

Run the following command and start styling:

    cd compass && compass watch

Screenshot(s)
~~~~~~~~~~~~~

.. image:: screenshot.png
    :alt: Screenshot of an article 
